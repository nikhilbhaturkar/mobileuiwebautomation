package com.testng.framework.testscripts;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.testng.framework.pageobjects.MUICDashboardPage;
import com.testng.framework.pageobjects.MUICLoginPage;
import com.testng.framework.utils.DriverInitializer;

public class LoginPageTest extends DriverInitializer{
	MUICLoginPage loginPage;
	MUICDashboardPage dashboardPage;
	
	@BeforeMethod
	public void setup() {
		loginPage = new MUICLoginPage(getDriver());
		dashboardPage=new MUICDashboardPage(getDriver());
	}
	
	@Test
	public void checkLogin() {
		loginPage.clickonLogin();
		dashboardPage.checkMenuonDashboard();
	}
	
	@AfterMethod
	public void quit() {
		quitBrowser();
	}
}
